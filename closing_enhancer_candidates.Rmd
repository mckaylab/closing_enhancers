```{r, setup}
library(magrittr)
library(ggplot2)
library(memes)
library(GenomicRanges)
library(BSgenome.Dmelanogaster.UCSC.dm3)
source('~/NystLib/R/peakUtils.R')

dm3 <- BSgenome.Dmelanogaster.UCSC.dm3

RNAseq <- read.csv('~/McKay/Appendage_Timepoints_RNASeq/RPKM_allgenes_alltimepoints_161019.csv',header=TRUE,sep=',')

union_chip_annot <- read.csv('../GEO_data/GSE141738_union_chip_peaks_annotated.csv', header = T, sep = ',')
union_chip_fracMax <- read.csv('../GEO_data/GSE141738_chip_peaks_fraction_max_FAIRE.csv', header = T, sep = ',')

load(file = '../closing_enhancer_candidates/R_Input/sens-dep_variables.rda')

E93.meme = '/Users/m/McKay/Motif_databases/E93_FlyFactor_Motif.meme'

```



Finding candidate closing sites from 3LW -> 24h 
  - Dependent on E93
  - Sensitive to GOF E93
  - Bound by E93 WT & GOF, or WT only
  - Decreasing 3LW -> 24h
  - Decreasing 3LW GOF -> 3LW WT
  - Cluster 2 behavior (closing and staying closed after 24h)

using SLN clustering and peak calls - pulled from GEO
directly from SLN's data instead of tables generated here
```{r}


cands <- dplyr::bind_cols(union_chip_annot, union_chip_fracMax) %>%
  dplyr::filter(cluster == 2
                & peak_binding_description == 'entopic' 
                & e93_dependent == 'dependent' 
                & e93_sensitive == 'sensitive'
                & e93_sensitive_behavior == 'Decreasing'
                & wildtype_3lw_24apf_behavior == 'Decreasing' |
                cluster == 1
                & peak_binding_description == 'entopic' 
                & e93_dependent == 'dependent' 
                & e93_sensitive == 'sensitive'
                & e93_sensitive_behavior == 'Decreasing'
                & wildtype_3lw_24apf_behavior == 'Decreasing' )

bed <- data.frame(cands$chr,
                  cands$start,
                  cands$end,
                  cands$id...5)  
cands

#write out a bed file that has all the 'candidate' enhancers - based on the above unionChip subsetting - useful for reference to a broad candidate group
write.table(bed, 'R_Output/e93_entopic_sens_dep_decreasing_cluster1-2.bed', sep = '\t', col.names = F, row.names = F, quote = F)

summary <- data.frame('cands' = nrow(cands))



```

making an annotated unionFairePeak df with categorizations for overlap with WT_closing,
GOF_closing, and mut_closing -- pull the log2fold values from deseq analysis in main FAIRE notebook
and annotate the overlap with the cands df (chunck above)
cands df -- is made from SLN annotated union E93 ChIP peak lists - available on GEO 
then subset by binding category, accessibility dynamics etc to select for regions most likely 
and dependent on E93 - and bound
```{r unionFairePeak}
#it would be nice to merge this with the motif analysis below -- DONE

#load the unionFairePeak_df and deseq contrast results made in the main FAIRE notebook

#annotate the unionFairePeaks df
unionFairePeaks_anno <- unionFairePeaks_df %>%
  dplyr::mutate(peak = glue::glue('peak_{num}', num = c(1:nrow(.))),
                WT_Closing = ifelse(GRanges(.) %over% GRanges(closing_WT_3LW_24h), TRUE, FALSE),
                GOF_Closing = ifelse(GRanges(.) %over% GRanges(closing_GOF_3LW_3LW), TRUE, FALSE),
                Mut_Static = ifelse(GRanges(.) %over% GRanges(closing_mut_3LW_24h), TRUE, FALSE)) %>%
  dplyr::mutate(peak_type = ifelse(.$WT_Closing == TRUE & .$Mut_Static == TRUE & .$GOF_Closing == TRUE, 'Sensitive_Dependent', 'NA')) %>%
  dplyr::mutate(Closing_Candidate = ifelse(GRanges(.) %over% GRanges(cands), 'candidate', 'NA')) %>%
  dplyr::mutate(WT_log2 = purrr::map_dbl(unionFairePeaks_df$start, function(x) 
      if(x %in% closing_WT_3LW_24h$start) {
        return(closing_WT_3LW_24h[closing_WT_3LW_24h$start == x,]$log2FoldChange)
      } else {
        return(0.0)  
      })) %>%
    dplyr::mutate(GOF_log2 = purrr::map_dbl(unionFairePeaks_df$start, function(x) 
      if(x %in% closing_GOF_3LW_3LW$start) {
        return(closing_GOF_3LW_3LW[closing_GOF_3LW_3LW$start == x,]$log2FoldChange)
      } else {
        return(0.0)  
      }))

#subset the union peak list by relevant categories -- 
uFP_candidates <- unionFairePeaks_anno %>%
  dplyr::filter(Closing_Candidate == 'candidate' & peak_type == 'Sensitive_Dependent') 
  


```
Fimo Scan candidate enhancers for E93 motif -- then append a count of motif hits per enhancer to the unionFairePeaks df
```{r}


#note that current memes requires full path - expansion bug

candidate_seqs <- uFP_candidates %>%
  GRanges() %>%
  get_sequence(dm3)

E93_motifs <- runFimo(candidate_seqs, E93.meme, thresh = 1e-3, silent = F)

# Visualize the sequences matching the E93 motif
plot_sequence_heatmap(E93_motifs$matched_sequence)

candidate_seqs@ranges
uFP_candidates %<>%
  plyranges::mutate(n_E93_motifs = plyranges::count_overlaps(GRanges(.), E93_motifs),
                    has_motif = n_E93_motifs > 0)

uFP_candidates
```

```{r annotate_enhancer_names}
enh_names <- data.frame(peaks = uFP_candidates$peak,
                        names = c("uif_a",
                                  "ana_a",
                                  "tou_a",
                                  "18w_a",
                                  "CG12078_a",
                                  "vvl_a",
                                  "pdp1_a",
                                  "ko_a",
                                  "corto_a",
                                  "e93_a",
                                  "irk2_a",
                                  "brDisc",
                                  "ovo_a"))

uFP_candidates %<>% 
  dplyr::mutate(enhancer_id = enh_names[enh_names$peaks == .$peak,]$names)
uFP_candidates
#write out a bed file
write.table(uFP_candidates[,c(2:4,17)], 'R_Output/unionFairePeaks_candidates.bed', col.names = F, row.names = F, sep = '\t', quote = F)

```
candidate enhancer sequences
```{r}
#GW <- seqinr::read.fasta(file = 'R_Input/GW.fasta', as.string = T, seqonly = T)

uFP_candidates %<>%
  dplyr::mutate(sequence = purrr::map_chr(uFP_candidates$peak, function(x) {
    Biostrings::getSeq(dm3, GRanges(uFP_candidates[uFP_candidates$peak == x,])) %>%
     as.character() 
    }))
  

#uFP_candidates %<>%
#  dplyr::mutate(sequence = purrr::map_chr(uFP_candidates$peak, function(x) {
#    Biostrings::getSeq(dm3, GRanges(uFP_candidates[uFP_candidates$peak == x,])) %>%
#     as.character() 
#    })) %>%
#  dplyr::mutate(GW_seq = glue::glue("{attL1}{candSeq}{attL2}", 
#           attL1 = GW[[1]],
#           attL2 = GW[[2]],
#           candSeq = .$sequence))
           
#write.table(uFP_candidates, 'R_Output/unionFairePeaks_candidates_sequences.csv', row.names = F, col.names = T, sep = ',', quote = F)

#seqinr::write() takes a list of sequences as input
#fasta_candidates <- uFP_candidates %>%
#  dplyr::mutate(sequence_list = lapply(.$sequence, function(x) x))

#seqinr::write.fasta(fasta_candidates$sequence_list,file.out = 'R_Output/closing_candidates.fa', names = fasta_candidates$peak, as.string = T)


#testing appending attL1 and attL2 to sequences
#GW <- seqinr::read.fasta(file = 'R_Input/GW.fasta', as.string = T, seqonly = T)

#uFP_candidates %>%
#  dplyr::mutate(GW_seq = glue::glue("{attL1}{candSeq}{attL2}", 
#           attL1 = GW[[1]],
#           attL2 = GW[[2]],
#           candSeq = uFP_candidates$sequence))
```

Plot signal at candidates
```{r cand_signal}
color_scale = c("#62c949","#4d9e39","#2e6122","#e64ed4","#ff8000")

bw_3lw <- "../BigWig/OR-3LW-Wing-FAIRE_3Reps_POOLED_zNorm.bw" %>% rtracklayer::BigWigFile()
bw_24 <- "../BigWig/OR-24APF-Wing-FAIRE_3Reps_POOLED_zNorm.bw" %>% rtracklayer::BigWigFile()
bw_44 <- "../BigWig/OR-44APF-Wing-FAIRE_3Reps_POOLED_zNorm.bw" %>% rtracklayer::BigWigFile()
bw_mut <- "../BigWig/E93mut-24APF-Wing-FAIRE_2Reps_POOLED_zNorm.bw" %>% rtracklayer::BigWigFile()
bw_gof <- "../BigWig/E93GOF-3LW-Wing-FAIRE_2Reps_POOLED_zNorm.bw" %>% rtracklayer::BigWigFile()

cand_peaks <- GRanges(uFP_candidates) 
cand_peaks <- plyranges::mutate(plyranges::anchor_center(cand_peaks), width = 2000)


peak_3lw <- signal::get_average_signal(bw_3lw, cand_peaks, by = "enhancer_id") %>% dplyr::mutate(signal = "3LW")
peak_24 <- signal::get_average_signal(bw_24, cand_peaks, by = "enhancer_id") %>% dplyr::mutate(signal = "24h")
peak_44 <- signal::get_average_signal(bw_44, cand_peaks, by = "enhancer_id") %>% dplyr::mutate(signal = "44h")
peak_gof <- signal::get_average_signal(bw_gof, cand_peaks, by = "enhancer_id") %>% dplyr::mutate(signal = "3LW_GOF")
peak_mut <- signal::get_average_signal(bw_mut, cand_peaks, by = "enhancer_id") %>% dplyr::mutate(signal = "24h_Mut")

cand_signal <- dplyr::bind_rows(peak_3lw,
                                peak_24,
                                peak_44,
                                peak_gof,
                                peak_mut) %>%
  dplyr::mutate(signal = factor(signal, levels = c("3LW","24h","44h","24h_Mut","3LW_GOF")))
  
cand_signal %>%
  ggplot(aes(x = position, y = mean, color = signal)) +
  geom_smooth(method = "loess", size = 1, span = 0.1) +
#  geom_point(color = "black", size = 0.1, alpha = 0.3) +
#  geom_line(size = 1) +
  ylab("FAIRE-seq (zNorm)") +
  ggtitle("FAIRE Siganl at Candidate Closing Enhancers") +
  scale_color_manual(values = color_scale) +
  facet_wrap(~id, scales = "free") +
  theme(axis.title.x = element_blank(),
        legend.title = element_blank()) +
  ggsave("R_Output/candidates_FAIRE.png", width = 8, height = 5) +
  NULL

```

```{r summary_table}
#Final.df <- data.frame(peaks = c('peak_3108', 'peak_10374', 'peak_21229', 'peak_21691', 'peak_35855', 'peak_36507', 'peak_42456'),
#                          name = c('uif.a_dm6', 'ana.a_dm6', 'vvl.a_dm6', 'pdp1.a_dm6', 'e93.a_dm6','irk2.a_dm6', 'ovo.a_dm6'))

final_list <- c('uif_a', 'ana_a', 'vvl_a', 'pdp1_a', 'e93_a','irk2_a', 'ovo_a')

final <- uFP_candidates %>%
  dplyr::filter(enhancer_id %in% final_list) %>%
  dplyr::mutate(sequence_list = lapply(.$sequence, function(x) x))

final_grob <- gridExtra::tableGrob(final[,c(17,2,3,4,5,13,14,15)], rows = NULL)

final
#write.table(final[,c(19,18)], 'R_Output/final_GeneWiz.csv', sep = ',', quote = F, row.names = F)
#write.table(final[,c(2,3,4,18)], 'R_Output/final_candidates.bed', sep = '\t', row.names = F, col.names = F, quote = F)

seqinr::write.fasta(final$sequence_list, file.out = 'R_Output/final_closing.fa', names = final$enhancer, as.string = T)

ggsave('R_Output/final_candidates.pdf', final_grob, width = 8, height = 5)

```



####### STOP ########
Below are old chunks for looking at gene expression - not part of current output
#####################


#
#
#finalizing candidate list - merging with chip list (SLN data)
#did some manual curating after reviewing in browser - several of these candidates I'd previously identified '../output_data/E93_sens_dep_close_bound.bed' 
#which is a list made from my own processing of FAIRE data - see other notebook in parent directory - and did make any call on GOF binding 
#```{r}
##this is a sheet I generated from another notebook and manually looking through browser
#
#candidates <- read.csv('Sheets/candidate_Sheet.csv')
#```
#
#```{r}
##RNAseq$Gene <- as.character(levels(RNAseq$Gene)[as.numeric(RNAseq$Gene)])
#
##function that gets specific gene 'g' from nested RNAseq data set and makes a new data frame 'a' with mean and sd for each timepoint.
##'g' must be a string
#get_gene <- function(g){
#  a <- by_gene %>%
#    subset(Gene == g) %>% 
#    tidyr::unnest() %>%
#    dplyr::group_by(Timepoint, Hours, Gene) %>%
#    dplyr::summarise(m = mean(RPKM), sd = sd(RPKM))
#  a$norm <- a$m/max(a$m)
#  return(a)
#}
#
##normalize loops through a given RNAseq datafram with 'Gene' variable, and normalizes mean RPKM values for each gene
#normalize <- function(b){
#  genes <- unique(b$Gene)
#  output <- data.frame(Gene = character(), 
#                       Timepoint = factor(),
#                       Hours = double(),
#                       Norm = double())
#  for (g in genes) {
#    x <- subset(b, Gene == g)
#    x$Norm <- x$m / max(x$m)
#    output <- bind_rows(output, x)
#  }
#  return(output)
#}
#
##separates the RNAseq data by replicates, making two dataframes
#rep1 <- RNAseq[c(1,2,4,6,8,10,12,14)]
#colnames(rep1) <- c('Gene','0','6','18','24','36','44','120')
#
#rep2 <- RNAseq[c(1,3,5,7,9,11,13,15)]
#colnames(rep2) <- c('Gene','0','6','18','24','36','44','120')
#
##binds the two replicates together (there's got to be a simpler way to do this)
#RNAseq.reps <- dplyr::bind_rows(rep1, rep2, .id = "Rep")                   
#
##melt the dataframe by Gene and Replicate, rename the variables
#RNAseq.reps <- reshape2::melt(RNAseq.reps, id = (c('Gene','Rep')))
#colnames(RNAseq.reps) <- c('Gene', 'Rep', 'Timepoint', 'RPKM')
#
##make new variable for timepoints as numeric double
#RNAseq.reps$Hours <- as.numeric(as.character(RNAseq.reps$Timepoint))
#
#
##make a nested data frame 'by_gene' that lets you search by gene name
#by_gene <- RNAseq.reps %>%
#  dplyr::group_by(Gene) %>%
#  tidyr::nest()
#
#
#```
#
#
#Looking at RPKM values at 3LW and 6h of manually annotated nearby genes to candidate enhancers
#```{r}
#closing_candidates <- cands %>% 
##closing_candidates <- dplyr::bind_cols(union_chip_annot, union_chip_fracMax) %>% 
#  dplyr::filter(id...5 %in% candidates$peak_id) %>%
#  dplyr::mutate(temp_name = unlist(lapply(.$id...5, function(x) candidates[candidates$peak_id == x,]$Symbol)),
#                RNAseq_id = unlist(lapply(.$id...5, function(x) candidates[candidates$peak_id == x,]$RNAseq_id)))
#
#closing_candidates_RNAseq <- closing_candidates %>%
#  dplyr::mutate(RNAseq = lapply(.$RNAseq_id, get_gene)) 
#
#closing_candidates %<>%
#  dplyr::mutate(RPKM_0h = unlist(lapply(closing_candidates_RNAseq$RNAseq, function(x) x[x$Timepoint == 0,]$m)),
#                RPKM_6h = unlist(lapply(closing_candidates_RNAseq$RNAseq, function(x) x[x$Timepoint == 6,]$m)),
#                RPKM_24h = unlist(lapply(closing_candidates_RNAseq$RNAseq, function(x) x[x$Timepoint == 24,]$m)),
#                Expressed = unlist(lapply(closing_candidates_RNAseq$RNAseq, function(x) if(x[x$Timepoint == '0',]$m >= 50 | x[x$Timepoint == '6',]$m >= 50) TRUE else FALSE)))
#
#cands
#
#bed_out <- closing_candidates[,c(1:3,20,5)]
#
##write.table(bed_out, 'closing_candidates.bed', sep = '\t', col.names = F, row.names = F, quote = F)
#```
#
#
#Not really necessary anymore with the above unionFairePeak_anno 
#Idea here is to take the candidate enhancer df and intersect with corresponding FAIRE peaks to get the opitmal ranges for cloning
#```{r annotate FAIRE coordinates}
#
#FAIRE_sheet <- read.csv('../PeakTable.tsv', header = T, sep = '\t') %>%
#  dplyr::mutate(grp = paste(Genotype,Type,Time, sep = '.'))
#
#Fpeaks <- FAIRE_sheet %>% 
#  getPeakData(by = 'grp', narrowPeak_colname = 'Peakfile') %>%
#  GRanges()
#
#Fpeaks_byGrp <- Fpeaks %>%
#  split(., mcols(.)$grp)
#
#
#candidates_FAIREranges <-  plyranges::join_overlap_inner(Fpeaks_byGrp$OR.WT.3LW, GRanges(closing_candidates_RNAseq)) %>%
#  data.frame()
#
#candidates_FAIREranges
##write.table(candidates_FAIREranges, 'closing_candidates_FAIREpeakRanges.tsv', sep = '\t', col.names = T, row.names = F, quote = F)
##write.table(candidates_FAIREranges[,c(1:3,33)], 'closing_candidates_FAIREpeakRanges.bed', sep = '\t', col.names = F, row.names = F, quote = F)
#
#```
#
#TO DO -- match temporary CRM names (based on nearest 'interesting' gene) to peak_ID -- DONE
#      -- make final table with info like - E93 motif presence etc. -- draft of this is 'candidates.xlsx' -- DONE
#      -- interesect peak regions with FAIRE peak regions to get full sequence of candidate -- DONE